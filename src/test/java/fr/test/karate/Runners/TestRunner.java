package fr.test.karate.Runners;

import com.intuit.karate.junit5.Karate;
import fr.test.karate.Hooks;
import fr.test.karate.config.PropertiesConfig;
/***/
public class TestRunner extends BaseRunner{

    private PropertiesConfig prop = PropertiesConfig.getInstance();

    @Karate.Test
    public Karate invoke( ){
        return Karate
                .run()
                .features(prop.getListFeatures())
                .hook(Hooks.getInstance())
                .reportDir(prop.getReportDir())
                .debugMode(false)
                .outputCucumberJson(true)
                .outputHtmlReport(true);
    }

}
