package fr.test.karate.Runners;

import fr.test.karate.report.Report;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;

public abstract class BaseRunner {
    /***
     *
     */
    @BeforeEach public void setUp(){
        // Try but doesn't work in local
        System.setProperty("allure.results.directory","target/allure-results");
    }
    /***
     * Generate Cucumber reporter
     */
    @AfterAll public static void tearDown(){
        //
        Report.getInstance().build();
    }

}
