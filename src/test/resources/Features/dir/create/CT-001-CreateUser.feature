@non-reg
Feature: US-001 - user creation

  Background:
    * url 'https://ztrain-shop.herokuapp.com'
    * header Accept = 'application/json'
    * header Content-type = "application/json"
    #
    # Objects
    #
    * def json = Java.type('fr.test.karate.dao.User')
    * def context = Java.type('fr.test.karate.dao.SetContext')
    # RANDOM USER = NULL
    # THROW IndexOfBoundException
    # var
    #
    * def jsonUser = json.getJsonHeader(null)
    * print jsonUser

  @createUser
  Scenario: User Creation

    Given path '/user/register'
    When request jsonUser
    And method post
    Then status 201
    And print response
    And match response.token == '#notnull'
    And def dummy = context.set('userToken', response.token )
    #
    #
    # print token
    And def username = context.set('username', response.user.email )
    And def userId = context.get('userToken')
    And print username
    And print userId