@non-reg
Feature: US-001 -  user creation


  Background:
    * url 'https://ztrain-shop.herokuapp.com'
    * header Accept = 'application/json'
    * header Content-type = "application/json"
    # Objects
    #
    * def json = Java.type('fr.test.karate.dao.User')
    * def payload = json.getJsonHeader(1)

  @nopCreateUser
  Scenario: try to create an user already registered
    Given path '/user/register'
    And request payload
    And method post
    Then status 400