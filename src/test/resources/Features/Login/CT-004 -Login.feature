@non-reg
Feature: US-001 - user login

  Background:
    * url 'https://ztrain-shop.herokuapp.com'
    * header Accept = 'application/json'
    * header Content-type = "application/json"
    #
    # Objects
    #
    * def User = Java.type('fr.test.karate.dao.User')
    * def context = Java.type('fr.test.karate.dao.SetContext')

  @login
  Scenario Outline: Connection user

    * def jsonUser = User.getUser('<username>', '<password>', <isContext>)
    * print jsonUser

    Given path '/auth/login'
    When request jsonUser
    And method post
    Then status 201
    And print response
    And match response.token == '#notnull'
    And def dummy = context.set('userToken', response.token )
    #
    #
    # print token
    And def userId = context.get('userToken')
    And print userId

    Examples:
      | username                | password                  | isContext |
      | username                |  username                 | true      |
      | mock_greg@provider.tld  | marcelmarcelmarcelmarcel  | false     |