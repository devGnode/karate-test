Feature: HealthCheck

  @health
  Scenario:
    Given url 'https://ztrain-shop.herokuapp.com'
    When method get
    Then status 200
    And match response == 'Hello World!'