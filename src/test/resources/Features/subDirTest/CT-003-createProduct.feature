Feature: US - Product creation


  Background:
    * url 'https://ztrain-shop.herokuapp.com'
    * header Accept = 'application/json'
    * header Content-type = "application/json"
    #
    * def context = Java.type('fr.test.karate.dao.SetContext')
    * def token = context.get('userToken')
    * header Authorization = "Bearer "+token
    #
    #
    * def Product = Java.type('fr.test.karate.dao.Product')
    * def payload = Product.getRandomWithBadPrice()

  @createProductBad
  Scenario: Create Product with a bad Price

    Given path '/product/create'

    And request payload
    And method post
    Then status 400

    And match response ==
    """
      {
       statusCode:'#number', message:'#ignore', error:"#string"
      }
    """

    And match response.message[0] == 'price must be an integer number'
    And match response.message[1] == 'price must not be less than 1'