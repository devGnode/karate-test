Feature: US - Product creation


  Background:
    * url 'https://ztrain-shop.herokuapp.com'
    * header Accept = 'application/json'
    * header Content-type = "application/json"
    #
    * def context = Java.type('fr.test.karate.dao.SetContext')
    * def token = context.get('userToken')
    * header Authorization = "Bearer "+token
    #
    #
    * def Product = Java.type('fr.test.karate.dao.Product')
    * def payload = Product.getRandomProduct()

  @createProduct
  Scenario: i create a product

    * def name = payload.name
    * def price = payload.price

    Given path '/product/create'
    And request payload
    And method post
    Then status 201
    And match response ==
    """
      {
       comments:'#ignore', price:'#number',name:'#string',description:'#string',createAt:"#string", image:'#string',_id:'#string',__v:"#number"
      }
    """

    # cast to Map<String, Object>
    # recording object
    And context.set('ResProduct',response)
