package fr.test.karate.utils;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class TreeDir {

    private int deep = 1;

    private int iterate;

    private File file = null;

    private Predicate<File> predicate = null;

    public TreeDir(File file){
        this.file       = file;
        this.iterate    = 0;
    }

    public TreeDir(File file, int deep){
        this.file       = file;
        this.deep       = deep;
        this.iterate    = 0;
    }

    public File getFile() { return file; }

    public int getDeep() { return deep; }

    public int getIterate() { return iterate; }

    private class PassPredicate implements Predicate<File>{
        @Override
        public boolean test(File file) { return true; }
    }

    public Predicate<File> getPredicate() { return !Objects.isNull(predicate) ? predicate : new PassPredicate(); }

    public TreeDir setPredicate(Predicate<File> predicate){
        this.predicate = predicate;
        return this;
    }

    public void read(Consumer<File> consumer){
        read0(this, consumer);
    }

    protected void read0( TreeDir self, Consumer<File> consumer){
        Iterator<File> files;
        File file;
        files = Arrays
                .asList(Objects.requireNonNull(self.getFile().listFiles()))
                .iterator();

        while (files.hasNext()){
            file = files.next();
            if( (file.isFile()&&getPredicate().test(file)) ) consumer.accept(file);
            if(file.isDirectory() && getIterate() < getDeep() ){
                iterate++;
                read0(new TreeDir(file, getDeep()){

                    @Override
                    public int getIterate() { return iterate; }

                    @Override
                    public Predicate<File> getPredicate() { return predicate; }

                }, consumer);
            }
        }

    }

    public static TreeDir of(String path){ return new TreeDir(new File(path)); }

    public static TreeDir of(String path, int deep ){ return new TreeDir(new File(path), deep); }
}
