package fr.test.karate.utils;

import java.security.SecureRandom;

public class RandString {

    private static final String alphaNumListing = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String RandomString(int length) {
        return RandString.RandomString(length, false);
    }

    public static String RandomString(int length, boolean onlyAlpha ) {
        SecureRandom random = new SecureRandom();
        char[] symbolsArray = alphaNumListing.substring(0, onlyAlpha ?  26 : alphaNumListing.length() ).toCharArray();
        char[] buffer = new char[length];

        for (int idx = 0; idx < buffer.length; ++idx)
            buffer[idx] = symbolsArray[random.nextInt(symbolsArray.length)];

        return new String(buffer);
    }
}
