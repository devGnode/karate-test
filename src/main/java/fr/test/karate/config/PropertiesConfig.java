package fr.test.karate.config;

import com.intuit.karate.core.Feature;
import fr.test.karate.utils.TreeDir;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class PropertiesConfig {
    /***
     * LOGGER
     */
    private final static Logger LOG = Logger.getLogger(PropertiesConfig.class);
    /***
     * SINGLETONPATTERN
     */
    private final static PropertiesConfig INSTANCE = new PropertiesConfig();

    private PropertiesLoader config = PropertiesLoader.of("config");

    public static int DEEP_FEATURE = 50;

    private String tags;

    private String features;

    private String reportDir;

    protected PropertiesConfig(){
        tags        = System.getProperty("tags",config.getProperty("tags",null));
        features    = System.getProperty("features",config.getProperty("features",null));
        reportDir   = System.getProperty("reportDir",config.getProperty("reportDir",null));
    }

    public String getReportDir(){ return reportDir; }

    public Optional<String> getTags(){ return Optional.ofNullable(tags); }

    public Optional<String> getFeatures(){ return Optional.ofNullable(features); }

    public String projectName(){ return config.getProperty("project.name"); }

    public Collection<Feature> getListFeatures(){
        List<Feature> out = new ArrayList<>();
        File file = new File(Objects.requireNonNull(features));

        // multiple file
        // corrupted path {File file}
        if( Arrays.asList(features.split(",")).size() > 1 ){
            return Arrays.asList(features.split(","))
                    .stream()
                    .map(value->Feature.read(value))
                    .collect(Collectors.toList());
        }
        // is a simple feature file
       if(file.isFile()) out.add(Feature.read(file));
       // is a directory
       else if(file.isDirectory()) TreeDir.of(file.toString(), DEEP_FEATURE).read(value -> out.add(Feature.read(value)));
       // is */**.feature
       else{
           AtomicReference<String> ext = new AtomicReference<>(".feature");

           if(file.getParent().endsWith("**")&&file.getName().startsWith("*")){
               DEEP_FEATURE = 50;
               ext.set(file.getName().replace("*",""));
               file =  file.getParentFile().getParentFile();
           }
           else if(file.getName().startsWith("*")){
               DEEP_FEATURE = 0;
               ext.set(file.getName().replace("*",""));
           }

            TreeDir.of(file.getParent(), DEEP_FEATURE)
                    .setPredicate(value-> value.isFile()&&value.getName().endsWith(ext.get()))
                    .read(value -> out.add(Feature.read(value)));
        }

        if(out.size() == 0) throw new RuntimeException("No features file found !");

        LOG.info("Feature file loaded = "+ out.size());
        out.stream().forEach(LOG::info);

        return out;
    }

    public static PropertiesConfig getInstance(){ return INSTANCE; }
    /***/
    public static PropertiesConfig init(){ return INSTANCE; }
}
