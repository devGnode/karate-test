package fr.test.karate.config;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Properties;

public class PropertiesLoader extends Properties {
    /***
     *
     */
    private final static String PROP_FILE = "config/%s.properties";
    /****
     * @constructor from String file
     * @param file
     */
    public PropertiesLoader(String file){
        try{ load(new BufferedReader(new FileReader(String.format(PROP_FILE,file)))); }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static PropertiesLoader of(String file){ return new PropertiesLoader(file); }
}
