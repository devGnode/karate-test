package fr.test.karate.dao;

import fr.test.karate.utils.RandString;
import org.json.JSONObject;

public class Product {

    public static int rand(int a, int b){
        return (int) ((Math.random()*b) + a);
    }

    private static JSONObject mountArticle(){
        JSONObject jo =  new JSONObject();

        jo.put("name",RandString.RandomString(rand(21,75),true));
        jo.put("description",RandString.RandomString(rand(100,200),true));
        jo.put("image","https://image.bb/IzjLozmx");
        jo.put("price",10/* rand(0,150)*/);

        System.out.println("PAYLOAD = "+jo.toString());

        return jo;
    }

    public static JSONObject getRandomProduct(){
        JSONObject jo =  new JSONObject();

        jo.put("name",RandString.RandomString(rand(21,75),true));
        jo.put("description",RandString.RandomString(rand(100,200),true));
        jo.put("image","https://image.bb/IzjLozmx");
        jo.put("price",10/* rand(0,150)*/);

        System.out.println("PAYLOAD = "+jo.toString());

        return jo;
    }

    public static JSONObject getRandomWithBadPrice(){
        JSONObject jo =  mountArticle();

        jo.put("price",0.0000000005);

        return jo;
    }
}
