package fr.test.karate.dao;

import fr.test.karate.context.Context;
import org.checkerframework.checker.units.qual.C;

public class SetContext {
    /***
     *
     */
    private static Context context = Context.getInstance();
    /***
     * @param name
     * @param value
     */
    public static void set(String name, Object value){
        context.set(name,value);
    }
    /***
     * @param name
     * @param <T>
     * @return
     */
    public static <T extends Object> T get(String name){
        return (T) context.get(name);
    }

}
