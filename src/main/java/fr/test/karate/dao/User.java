package fr.test.karate.dao;

import fr.test.karate.utils.RandString;
import jdk.nashorn.internal.scripts.JO;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

public class User {
    /***
     *
     */
    private static final String jsonUserPath = "./src/test/resources/user.json";
    /***
     *
     */
    public static JSONObject randUser(){
        JSONObject jo =  new JSONObject();
        int sz = (int) ((Math.random()*50) + 10);
        String tmp = RandString.RandomString(sz,true);

        jo.put("email",tmp.toLowerCase()+"@yopmail.com");
        jo.put("password",tmp);
        jo.put("adress",tmp);
        jo.put("age", (int)((Math.random()*60)+1));
        System.out.println("PAYLOAD = "+jo.toString());

        return jo;
    }
    /***
     * @param user
     * @return
     */
    public static JSONObject getJsonHeader( Integer user )  {

        // Random User
        // user is null
        if(Objects.isNull(user))return User.randUser();
        else{
            JSONObject jo;
            StringBuffer buffer = new StringBuffer();

            try(BufferedReader br = new BufferedReader(new FileReader(jsonUserPath))){

                String tmp;
                while((tmp=br.readLine())!=null)buffer.append(tmp);

                jo = new JSONObject(buffer.toString());
            }catch (Exception e){
                throw new RuntimeException("");
            }

            //output data
            JSONObject out = new JSONObject(jo.getJSONArray("users").get(user).toString());
            System.out.println(String.format("Payload send = [%s] ", out.toString()));

            return out;
        }
    }

    public static JSONObject getUser(String name, String password, boolean isRandom){
        JSONObject jo =  new JSONObject();

        if(isRandom){
            String username = SetContext.get(name);

            jo.put("email",username.toLowerCase());
            jo.put("password",username.replace("@yopmail.com","").toUpperCase());

            return jo;
        }else{
            jo.put("email",name);
            jo.put("password",password);
        }

        return jo;
    }
}
