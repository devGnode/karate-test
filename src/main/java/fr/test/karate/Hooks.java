package fr.test.karate;

import com.intuit.karate.RuntimeHook;
import com.intuit.karate.core.FeatureRuntime;
import com.intuit.karate.core.ScenarioRuntime;
import com.intuit.karate.core.Step;
import com.intuit.karate.core.StepResult;
import fr.test.karate.report.Report;
import io.qameta.allure.Allure;
import io.qameta.allure.model.Attachment;
import io.qameta.allure.model.Status;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;

public class Hooks implements RuntimeHook {

    private final static Hooks INSTANCE = new Hooks();

    private final Report report = Report.getInstance();

    protected Hooks(){ }

    @Override
    public boolean beforeFeature(FeatureRuntime fr) {
      //  System.out.println(fr.feature.getResource().getFileNameWithoutExtension() + " / " + fr.feature.getKarateJsonFileName().replace(".txt",".json") + " / " + fr.result.getFeature().getName());

        Allure.suite(fr.feature.getName());
        Allure.description(fr.feature.getDescription());
        return true;
    }

    @Override
    public boolean beforeScenario(ScenarioRuntime sr) {
        Allure.feature(sr.featureRuntime.feature.getResource().getFile().toString());
        Allure.descriptionHtml(sr.scenario.getDescription());
        return true;
    }

    @Override
    public void afterScenario(ScenarioRuntime sr) {
        sr.scenario.getTags().stream().forEach(value-> Allure.label("tags","@"+value.getName()));
    }

    /***
     * Allure beforeStep
     */
    @Override
    public boolean beforeStep(Step step, ScenarioRuntime sr) {
        return true;
    }

    /***
     * Allure AfterStep
     * @param result
     * @param sr
     */
    @Override
    public void afterStep(StepResult result, ScenarioRuntime sr) {
        Status state = Status.PASSED;
        StringBuffer buffer = new StringBuffer();

        // state
        if(result.getResult().isFailed()) state     = Status.FAILED;
        if(result.getResult().isAborted()) state    = Status.BROKEN;
        if(result.getResult().isSkipped()) state    = Status.SKIPPED;
        // step
        buffer.append(result.getStep().getPrefix())
                .append(" ")
                .append(result.getStep().getText());
        Allure.step(buffer.toString(), state);
        String tmp = result.getStepLog();

        if( tmp.length() > 0 ){

            Allure.getLifecycle().updateTestCase( testResult ->{

                try {
                    String file = UUID.randomUUID()+"-attachement.txt";
                    BufferedWriter fileWriter = new BufferedWriter(new FileWriter("allure-results/"+file));
                    fileWriter.write(tmp);
                    fileWriter.close();

                    testResult.getSteps()
                            .get(result.getStep().getIndex())
                            .setAttachments(Arrays.asList(new Attachment().setType("text/plain").setName(file).setSource(file)));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            });

        }

    }

    @Override
    public void afterFeature(FeatureRuntime fr) {
        // set scenario filename for cucumberJSON
        //report.setJsonFiles(fr.feature.getKarateJsonFileName());
    }

    public static Hooks getInstance(){ return INSTANCE; }
}
