package fr.test.karate.context;

import java.util.HashMap;

public class Context {
    /***
     *
     */
    private static final Context INSTANCE = new Context();
    /***
     *
     */
    private HashMap<String, Object> data = new HashMap();
    /***
     * @param name
     * @param key
     * @param <T>
     */
    public <T extends Object> void set(String name, T key){
        data.put(name,key);
    }
    /***
     * @param name
     * @param <T>
     * @return
     */
    public <T> T get(String name){
        return (T) data.get(name);
    }
    /***
     * @return
     */
    public static Context getInstance(){ return INSTANCE; }
}
